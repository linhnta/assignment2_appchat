// Code generated by entc, DO NOT EDIT.

package groupmessage

const (
	// Label holds the string label denoting the groupmessage type in the database.
	Label = "group_message"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldText holds the string denoting the text field in the database.
	FieldText = "text"
	// FieldTime holds the string denoting the time field in the database.
	FieldTime = "time"
	// EdgeSent holds the string denoting the sent edge name in mutations.
	EdgeSent = "sent"
	// EdgeReceived holds the string denoting the received edge name in mutations.
	EdgeReceived = "received"
	// Table holds the table name of the groupmessage in the database.
	Table = "group_messages"
	// SentTable is the table that holds the sent relation/edge.
	SentTable = "group_messages"
	// SentInverseTable is the table name for the User entity.
	// It exists in this package in order to avoid circular dependency with the "user" package.
	SentInverseTable = "users"
	// SentColumn is the table column denoting the sent relation/edge.
	SentColumn = "user_sends"
	// ReceivedTable is the table that holds the received relation/edge.
	ReceivedTable = "group_messages"
	// ReceivedInverseTable is the table name for the Groupchat entity.
	// It exists in this package in order to avoid circular dependency with the "groupchat" package.
	ReceivedInverseTable = "groupchats"
	// ReceivedColumn is the table column denoting the received relation/edge.
	ReceivedColumn = "groupchat_receives"
)

// Columns holds all SQL columns for groupmessage fields.
var Columns = []string{
	FieldID,
	FieldText,
	FieldTime,
}

// ForeignKeys holds the SQL foreign-keys that are owned by the "group_messages"
// table and are not defined as standalone fields in the schema.
var ForeignKeys = []string{
	"groupchat_receives",
	"user_sends",
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	for i := range ForeignKeys {
		if column == ForeignKeys[i] {
			return true
		}
	}
	return false
}
