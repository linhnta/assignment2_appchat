package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Groupchat holds the schema definition for the Groupchat entity.
type Groupchat struct {
	ent.Schema
}

// Fields of the Groupchat.
func (Groupchat) Fields() []ent.Field {
	return []ent.Field{
		field.String("GroupName").Default("GroupName"),
	}
}

// Edges of the Groupchat.
func (Groupchat) Edges() []ent.Edge {
	return []ent.Edge{
		//group users
		edge.To("users", User.Type),
		//receive messages
		edge.To("receives", GroupMessage.Type),
	}

}
