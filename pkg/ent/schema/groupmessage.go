package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// GroupMessage holds the schema definition for the GroupMessage entity.
type GroupMessage struct {
	ent.Schema
}

// Fields of the GroupMessage.
func (GroupMessage) Fields() []ent.Field {
	return []ent.Field{
		field.String("Text"),
		field.String("Time"),
	}
}

// Edges of the GroupMessage.
func (GroupMessage) Edges() []ent.Edge {
	return []ent.Edge{
		//user send message
		edge.From("sent", User.Type).Ref("sends").Unique(),
		//receive message
		edge.From("received", Groupchat.Type).Ref("receives").Unique(),
	}
}
