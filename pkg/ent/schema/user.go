package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("username").NotEmpty().Unique().Immutable(),
		field.String("password"),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return []ent.Edge{
		//send message
		// edge.To("sends", Message.Type),
		//receive messages
		// edge.To("receives", Message.Type),
		//group users
		edge.From("groups", Groupchat.Type).Ref("users"),
		//send group message
		edge.To("sends", GroupMessage.Type),
	}
}
