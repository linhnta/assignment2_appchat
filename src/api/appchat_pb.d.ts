import * as jspb from 'google-protobuf'



export class Message extends jspb.Message {
  getUsername(): string;
  setUsername(value: string): Message;

  getChannel(): string;
  setChannel(value: string): Message;

  getContent(): string;
  setContent(value: string): Message;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Message.AsObject;
  static toObject(includeInstance: boolean, msg: Message): Message.AsObject;
  static serializeBinaryToWriter(message: Message, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Message;
  static deserializeBinaryFromReader(message: Message, reader: jspb.BinaryReader): Message;
}

export namespace Message {
  export type AsObject = {
    username: string,
    channel: string,
    content: string,
  }
}

export class Messages extends jspb.Message {
  getCreateChannelRequest(): CreateChannelRequest | undefined;
  setCreateChannelRequest(value?: CreateChannelRequest): Messages;
  hasCreateChannelRequest(): boolean;
  clearCreateChannelRequest(): Messages;

  getCreateChannelReply(): CreateChannelReply | undefined;
  setCreateChannelReply(value?: CreateChannelReply): Messages;
  hasCreateChannelReply(): boolean;
  clearCreateChannelReply(): Messages;

  getJoinRequest(): JoinRequest | undefined;
  setJoinRequest(value?: JoinRequest): Messages;
  hasJoinRequest(): boolean;
  clearJoinRequest(): Messages;

  getJoinReply(): JoinReply | undefined;
  setJoinReply(value?: JoinReply): Messages;
  hasJoinReply(): boolean;
  clearJoinReply(): Messages;

  getLeaveRequest(): LeaveRequest | undefined;
  setLeaveRequest(value?: LeaveRequest): Messages;
  hasLeaveRequest(): boolean;
  clearLeaveRequest(): Messages;

  getLeaveReply(): LeaveReply | undefined;
  setLeaveReply(value?: LeaveReply): Messages;
  hasLeaveReply(): boolean;
  clearLeaveReply(): Messages;

  getStateRequest(): StateRequest | undefined;
  setStateRequest(value?: StateRequest): Messages;
  hasStateRequest(): boolean;
  clearStateRequest(): Messages;

  getStateReply(): StateReply | undefined;
  setStateReply(value?: StateReply): Messages;
  hasStateReply(): boolean;
  clearStateReply(): Messages;

  getChatRequest(): ChatRequest | undefined;
  setChatRequest(value?: ChatRequest): Messages;
  hasChatRequest(): boolean;
  clearChatRequest(): Messages;

  getChatReply(): ChatReply | undefined;
  setChatReply(value?: ChatReply): Messages;
  hasChatReply(): boolean;
  clearChatReply(): Messages;

  getPayloadCase(): Messages.PayloadCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Messages.AsObject;
  static toObject(includeInstance: boolean, msg: Messages): Messages.AsObject;
  static serializeBinaryToWriter(message: Messages, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Messages;
  static deserializeBinaryFromReader(message: Messages, reader: jspb.BinaryReader): Messages;
}

export namespace Messages {
  export type AsObject = {
    createChannelRequest?: CreateChannelRequest.AsObject,
    createChannelReply?: CreateChannelReply.AsObject,
    joinRequest?: JoinRequest.AsObject,
    joinReply?: JoinReply.AsObject,
    leaveRequest?: LeaveRequest.AsObject,
    leaveReply?: LeaveReply.AsObject,
    stateRequest?: StateRequest.AsObject,
    stateReply?: StateReply.AsObject,
    chatRequest?: ChatRequest.AsObject,
    chatReply?: ChatReply.AsObject,
  }

  export enum PayloadCase { 
    PAYLOAD_NOT_SET = 0,
    CREATE_CHANNEL_REQUEST = 1,
    CREATE_CHANNEL_REPLY = 2,
    JOIN_REQUEST = 3,
    JOIN_REPLY = 4,
    LEAVE_REQUEST = 5,
    LEAVE_REPLY = 6,
    STATE_REQUEST = 7,
    STATE_REPLY = 8,
    CHAT_REQUEST = 9,
    CHAT_REPLY = 10,
  }
}

export class User extends jspb.Message {
  getUsername(): string;
  setUsername(value: string): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): User.AsObject;
  static toObject(includeInstance: boolean, msg: User): User.AsObject;
  static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): User;
  static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
  export type AsObject = {
    username: string,
  }
}

export class Message_CHAN extends jspb.Message {
  getUsername(): string;
  setUsername(value: string): Message_CHAN;

  getChannelName(): string;
  setChannelName(value: string): Message_CHAN;

  getBody(): string;
  setBody(value: string): Message_CHAN;

  getTime(): string;
  setTime(value: string): Message_CHAN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Message_CHAN.AsObject;
  static toObject(includeInstance: boolean, msg: Message_CHAN): Message_CHAN.AsObject;
  static serializeBinaryToWriter(message: Message_CHAN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Message_CHAN;
  static deserializeBinaryFromReader(message: Message_CHAN, reader: jspb.BinaryReader): Message_CHAN;
}

export namespace Message_CHAN {
  export type AsObject = {
    username: string,
    channelName: string,
    body: string,
    time: string,
  }
}

export class SendMessageRequest extends jspb.Message {
  getText(): string;
  setText(value: string): SendMessageRequest;

  getTime(): string;
  setTime(value: string): SendMessageRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendMessageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendMessageRequest): SendMessageRequest.AsObject;
  static serializeBinaryToWriter(message: SendMessageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendMessageRequest;
  static deserializeBinaryFromReader(message: SendMessageRequest, reader: jspb.BinaryReader): SendMessageRequest;
}

export namespace SendMessageRequest {
  export type AsObject = {
    text: string,
    time: string,
  }
}

export class CreateChannelRequest extends jspb.Message {
  getChannelName(): string;
  setChannelName(value: string): CreateChannelRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateChannelRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateChannelRequest): CreateChannelRequest.AsObject;
  static serializeBinaryToWriter(message: CreateChannelRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateChannelRequest;
  static deserializeBinaryFromReader(message: CreateChannelRequest, reader: jspb.BinaryReader): CreateChannelRequest;
}

export namespace CreateChannelRequest {
  export type AsObject = {
    channelName: string,
  }
}

export class CreateChannelReply extends jspb.Message {
  getChannelName(): string;
  setChannelName(value: string): CreateChannelReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateChannelReply.AsObject;
  static toObject(includeInstance: boolean, msg: CreateChannelReply): CreateChannelReply.AsObject;
  static serializeBinaryToWriter(message: CreateChannelReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateChannelReply;
  static deserializeBinaryFromReader(message: CreateChannelReply, reader: jspb.BinaryReader): CreateChannelReply;
}

export namespace CreateChannelReply {
  export type AsObject = {
    channelName: string,
  }
}

export class JoinRequest extends jspb.Message {
  getChannelName(): string;
  setChannelName(value: string): JoinRequest;

  getUsername(): string;
  setUsername(value: string): JoinRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): JoinRequest.AsObject;
  static toObject(includeInstance: boolean, msg: JoinRequest): JoinRequest.AsObject;
  static serializeBinaryToWriter(message: JoinRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): JoinRequest;
  static deserializeBinaryFromReader(message: JoinRequest, reader: jspb.BinaryReader): JoinRequest;
}

export namespace JoinRequest {
  export type AsObject = {
    channelName: string,
    username: string,
  }
}

export class JoinReply extends jspb.Message {
  getChannelName(): string;
  setChannelName(value: string): JoinReply;

  getUsername(): string;
  setUsername(value: string): JoinReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): JoinReply.AsObject;
  static toObject(includeInstance: boolean, msg: JoinReply): JoinReply.AsObject;
  static serializeBinaryToWriter(message: JoinReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): JoinReply;
  static deserializeBinaryFromReader(message: JoinReply, reader: jspb.BinaryReader): JoinReply;
}

export namespace JoinReply {
  export type AsObject = {
    channelName: string,
    username: string,
  }
}

export class LeaveRequest extends jspb.Message {
  getChannelName(): string;
  setChannelName(value: string): LeaveRequest;

  getUsername(): string;
  setUsername(value: string): LeaveRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeaveRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LeaveRequest): LeaveRequest.AsObject;
  static serializeBinaryToWriter(message: LeaveRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeaveRequest;
  static deserializeBinaryFromReader(message: LeaveRequest, reader: jspb.BinaryReader): LeaveRequest;
}

export namespace LeaveRequest {
  export type AsObject = {
    channelName: string,
    username: string,
  }
}

export class LeaveReply extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): LeaveReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeaveReply.AsObject;
  static toObject(includeInstance: boolean, msg: LeaveReply): LeaveReply.AsObject;
  static serializeBinaryToWriter(message: LeaveReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeaveReply;
  static deserializeBinaryFromReader(message: LeaveReply, reader: jspb.BinaryReader): LeaveReply;
}

export namespace LeaveReply {
  export type AsObject = {
    status: string,
  }
}

export class StateRequest extends jspb.Message {
  getChannelName(): string;
  setChannelName(value: string): StateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StateRequest): StateRequest.AsObject;
  static serializeBinaryToWriter(message: StateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StateRequest;
  static deserializeBinaryFromReader(message: StateRequest, reader: jspb.BinaryReader): StateRequest;
}

export namespace StateRequest {
  export type AsObject = {
    channelName: string,
  }
}

export class StateReply extends jspb.Message {
  getUsersList(): Array<User>;
  setUsersList(value: Array<User>): StateReply;
  clearUsersList(): StateReply;
  addUsers(value?: User, index?: number): User;

  getMessagesList(): Array<Message_CHAN>;
  setMessagesList(value: Array<Message_CHAN>): StateReply;
  clearMessagesList(): StateReply;
  addMessages(value?: Message_CHAN, index?: number): Message_CHAN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StateReply.AsObject;
  static toObject(includeInstance: boolean, msg: StateReply): StateReply.AsObject;
  static serializeBinaryToWriter(message: StateReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StateReply;
  static deserializeBinaryFromReader(message: StateReply, reader: jspb.BinaryReader): StateReply;
}

export namespace StateReply {
  export type AsObject = {
    usersList: Array<User.AsObject>,
    messagesList: Array<Message_CHAN.AsObject>,
  }
}

export class ChatRequest extends jspb.Message {
  getUsername(): string;
  setUsername(value: string): ChatRequest;

  getChannelName(): string;
  setChannelName(value: string): ChatRequest;

  getText(): string;
  setText(value: string): ChatRequest;

  getTime(): string;
  setTime(value: string): ChatRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChatRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ChatRequest): ChatRequest.AsObject;
  static serializeBinaryToWriter(message: ChatRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChatRequest;
  static deserializeBinaryFromReader(message: ChatRequest, reader: jspb.BinaryReader): ChatRequest;
}

export namespace ChatRequest {
  export type AsObject = {
    username: string,
    channelName: string,
    text: string,
    time: string,
  }
}

export class ChatReply extends jspb.Message {
  getUsername(): string;
  setUsername(value: string): ChatReply;

  getChannelName(): string;
  setChannelName(value: string): ChatReply;

  getText(): string;
  setText(value: string): ChatReply;

  getTime(): string;
  setTime(value: string): ChatReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChatReply.AsObject;
  static toObject(includeInstance: boolean, msg: ChatReply): ChatReply.AsObject;
  static serializeBinaryToWriter(message: ChatReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChatReply;
  static deserializeBinaryFromReader(message: ChatReply, reader: jspb.BinaryReader): ChatReply;
}

export namespace ChatReply {
  export type AsObject = {
    username: string,
    channelName: string,
    text: string,
    time: string,
  }
}

