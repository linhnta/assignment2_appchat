/**
 * @fileoverview gRPC-Web generated client stub for api
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as api_appchat_pb from '../api/appchat_pb';


export class appchatClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodDescriptorLogin = new grpcWeb.MethodDescriptor(
    '/api.appchat/Login',
    grpcWeb.MethodType.UNARY,
    api_appchat_pb.LoginRequest,
    api_appchat_pb.LoginReply,
    (request: api_appchat_pb.LoginRequest) => {
      return request.serializeBinary();
    },
    api_appchat_pb.LoginReply.deserializeBinary
  );

  login(
    request: api_appchat_pb.LoginRequest,
    metadata: grpcWeb.Metadata | null): Promise<api_appchat_pb.LoginReply>;

  login(
    request: api_appchat_pb.LoginRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: api_appchat_pb.LoginReply) => void): grpcWeb.ClientReadableStream<api_appchat_pb.LoginReply>;

  login(
    request: api_appchat_pb.LoginRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: api_appchat_pb.LoginReply) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/api.appchat/Login',
        request,
        metadata || {},
        this.methodDescriptorLogin,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/api.appchat/Login',
    request,
    metadata || {},
    this.methodDescriptorLogin);
  }

}

