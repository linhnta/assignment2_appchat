import * as pb from './api/appchat_pb'

// var ws = new WebSocket('ws://localhost:9000/ws');
// var ws = new WebSocket("ws://" + document.location.host + "/ws");

window.onload = () => {
  if (window["WebSocket"]) {
  var ws = new WebSocket('ws://localhost:9000/ws?user='+ getUsername()+ "&channel=" + getChannelName());
  ws.binaryType="arraybuffer"
  ws.onclose = function (evt) {
      var item = document.createElement("div");
      item.innerHTML = "<b>Connection closed.</b>";
      appendLog(item);
  };
  ws.onmessage = function (evt) {
   
      var messages = evt.data
      console.log(messages);
      // var reply = new pb.Messages;
      var msg_rel = pb.Messages.deserializeBinary(messages);
      
      console.log("rel: ", msg_rel)
    
      // for (var i = 0; i < messages.length; i++) {
          var item = document.createElement("div");
          item.innerText = msg_rel.getChatReply().getUsername() + ": " +   msg_rel.getChatReply().getText();
          appendLog(item);
      // }
  };
}
  // var ws = new WebSocket("ws://" + document.location.host + "/ws");
  var btnConnect = document.getElementById("connect");
  var btnJoin = document.getElementById("join");
  var btnLeave = document.getElementById("leave");
  var btnSend = document.getElementById("send");
  var log = document.getElementById("log")

  let dateTime = new Date();
  var curDate = dateTime.toString();

  function appendLog(item: any) {
    var doScroll = log.scrollTop > log.scrollHeight - log.clientHeight - 1;
    log.appendChild(item);
    if (doScroll) {
        log.scrollTop = log.scrollHeight - log.clientHeight;
    }
}
  //click to login

    //   btnLogin.addEventListener("click", function() {

        
    //   var loginReq = new pb.LoginRequest();
    //   loginReq.setUsername(getUsername()).setPassword(getPassword());
      
    //   console.log("login: buf %v: ", loginReq.getUsername());
    //   //byte

    //   var buf = loginReq.serializeBinary();

    //   //send message loginrequest
    //   ws.send(buf);

    //   console.log("login: buf %v: ", buf);

    //   //
    //   var req2 = pb.LoginRequest.deserializeBinary(buf);
    //   console.log("login: %v", req2.getUsername())
    // });

    //click to connect: connect channel to send message

    // btnConnect.addEventListener("click", function() {
    //   var connectReq;
    //   connectReq.setUsername(txtusername).setPassword(txtpassword);
      
    //   //byte

    //   var buf = connectReq.serializeBinary();

    //   //send message loginrequest
    //   ws.send(buf);

    //   console.log("buf %v: ", buf);
    // });

    //click to join 

    btnJoin.addEventListener("click", function() {
      
      var joinReq = new pb.JoinRequest();
      joinReq.setChannelName(getChannelName()).setUsername(getUsername());
     
      var message = new pb.Messages;
      message.setJoinRequest(joinReq);

      //byte

      var buf = message.serializeBinary();

      //send message joinrequest
      ws.send(buf);

      console.log("join: buf %v: ", buf);
    });

    //click to leave
    btnLeave.addEventListener("click", function() {
      
      var leaveReq = new pb.LeaveRequest();
      leaveReq.setChannelName(getChannelName()).setUsername(getUsername());
    
      var message = new pb.Messages;
      message.setJoinRequest(leaveReq);
      //byte

      var buf = message.serializeBinary();

      //send message leaverequest
      ws.send(buf);

      console.log("leave: buf %v: ", buf);
    });

    //click to send
    btnSend.addEventListener("click", function() {
     
      var sendReq = new pb.ChatRequest();
      sendReq.setUsername(getUsername()).setChannelName(getChannelName()).setText(getMsg()).setTime(curDate);
     
      var message = new pb.Messages;
      message.setChatRequest(sendReq);
      //byte
      var buf = message.serializeBinary();

      //send message leaverequest
      ws.send(buf);

      console.log("send: buf %v: ", buf);

    });

}

function getUsername(): string {
    return (<HTMLInputElement>document.getElementById("username")).value;
}

// function getPassword(): string {
//   return (<HTMLInputElement>document.getElementById("password")).value;
// }

function getChannelName(): string {
  return (<HTMLInputElement>document.getElementById("channel")).value;
}

function getMsg(): string {
  return (<HTMLInputElement>document.getElementById("msg")).value;
}