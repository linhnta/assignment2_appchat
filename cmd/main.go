package main

import (
	"context"
	"flag"

	// "net"
	"net/http"

	// "flag"
	// "fmt"
	"log"

	// "net/http"

	"gitlab.com/linhnta/assignment2_appchat/pkg/ent"
	// "google.golang.org/grpc"

	// pb "gitlab.com/linhnta/assignment2_appchat/api"
	h "gitlab.com/linhnta/assignment2_appchat/internal"

	_ "github.com/go-sql-driver/mysql"
)

var addr = flag.String("addr", ":9000", "http service address")

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "../dist/index.html")
}

// const (
// 	port = ":8080"
// )

// type AppchatServer struct {
// 	pb.UnimplementedAppchatServer
// 	db *ent.Client
// }

func main() {

	//connect db
	client, errDB := ent.Open("mysql", "root:admin@tcp(localhost:3306)/appchat?parseTime=True")
	if errDB != nil {
		log.Fatalf("failed opening connection to mysql: %v", errDB)
	}
	defer client.Close()
	// Run the auto migration tool.
	if errS := client.Schema.Create(context.Background()); errS != nil {
		log.Fatalf("failed creating schema resources: %v", errS)
	}

	//websocket
	flag.Parse()
	hub := h.NewHub(client)
	go hub.Run()
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		h.ServeWs(hub, w, r)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

	// gRPC
	// lis, err := net.Listen("tcp", port)
	// if err != nil {
	// 	log.Fatalf("failed to listen: %v", err)
	// }
	// s := grpc.NewServer()

	// pb.RegisterAppchatServer(s, &AppchatServer{pb.UnimplementedAppchatServer{}, client})

	// log.Printf("server listening at %v", lis.Addr())
	// if err := s.Serve(lis); err != nil {
	// 	log.Fatalf("failed to serve: %v", err)
	// }
}

// // register
// func (s *AppchatServer) Register(ctx context.Context, in *pb.RegisterRequest) (*pb.RegisterReply, error) {
// 	user, err := s.db.User.Create().
// 		SetUsername(in.Username).
// 		SetPassword(in.Password).
// 		Save(ctx)
// 	if err != nil {
// 		return nil, fmt.Errorf("error ... : %v", err)
// 	}
// 	return &pb.RegisterReply{
// 			Username: user.Username,
// 		},
// 		nil

// }
