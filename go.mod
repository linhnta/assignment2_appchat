module gitlab.com/linhnta/assignment2_appchat

go 1.18

require (
	entgo.io/ent v0.10.2-0.20220502113020-4ac82f5bb3f0
	github.com/go-sql-driver/mysql v1.6.0
)

require (
	ariga.io/atlas v0.4.2 // indirect
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/go-openapi/inflect v0.19.0 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.5.0
	github.com/hashicorp/hcl/v2 v2.12.0 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/zclconf/go-cty v1.10.0 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/protobuf v1.28.0
)
