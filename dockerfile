#build stage

FROM golang:latest


RUN mkdir /app


WORKDIR /app

# COPY go.mod /app/assignment2_appchat/

# COPY go.sum /app/assignment2_appchat/


COPY . /app/assignment2_appchat/


RUN cd /app/assignment2_appchat && go mod download all


RUN cd /app/assignment2_appchat/cmd && go build 


EXPOSE 8080



CMD ["/app/assignment2_appchat/cmd/cmd"]
