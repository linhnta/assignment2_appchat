package internal

import (
	// "context"
	// "encoding/json"
	// "fmt"
	// "log"
	// "strconv"
	// "time"

	"context"

	"fmt"
	"log"
	"strconv"

	pb "gitlab.com/linhnta/assignment2_appchat/api"
	"gitlab.com/linhnta/assignment2_appchat/pkg/ent"

	// "gitlab.com/linhnta/assignment2_appchat/pkg/ent/user"
	"google.golang.org/protobuf/proto"
	//  pb "gitlab.com/linhnta/assignment2_appchat/src/api"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients. ----All of clients
	clients map[*Client]bool

	channels map[string][]*Client

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	db *ent.Client
}

func NewHub(db *ent.Client) *Hub {
	return &Hub{
		db:         db,
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
		channels:   make(map[string][]*Client),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			var m pb.Messages

			err1 := proto.Unmarshal(message, &m)
			if err1 != nil {
				log.Fatal("Fail to decode message in run: ", err1)
			}
			request := m.GetChatRequest()
			fmt.Println("message hub: %v", request)

			//check username in user, if username exists then check user channel, if userchannel exists then
			//insert message_channel. else if username not exists -> insert into user, insert into channel and user channel.
			//then insert into message_channel

			// select all user
			users, _ := h.db.User.Query().All(context.Background())

			var temp1 = "0"
			intTemp1, _ := strconv.Atoi(temp1)

			for _, user := range users {
				if user.Username == request.Username {
					// check channel user

					// check channel
					channels, err_channel := h.db.Groupchat.Query().All(context.Background())
					if err_channel != nil {
						_, errCreateChannel := h.db.Groupchat.Create().SetGroupName(request.ChannelName).Save(context.Background())

						if errCreateChannel != nil {
							log.Fatal("Fail to add channel to db: ", errCreateChannel)
						}
					}

					var temp = "0"
					intTemp, _ := strconv.Atoi(temp)

					for _, channel := range channels {
						if channel.GroupName == request.ChannelName {
							_, errCreate := h.db.GroupMessage.Create().SetText(request.Text).SetTime(request.Time).SetReceivedID(channel.ID).SetSentID(user.ID).Save(context.Background())
							if errCreate != nil {
								log.Fatal("Fail to add message to db: ", errCreate)
							}
							break

						} else {
							intTemp = intTemp + 1
						}
					}

					if len(channels) == intTemp {
						_, errCreateChannel := h.db.Groupchat.Create().SetGroupName(request.ChannelName).Save(context.Background())

						if errCreateChannel != nil {
							log.Fatal("Fail to add channel to db: ", errCreateChannel)
						}

						var channel_ids = ""
						channel_id, _ := strconv.Atoi(channel_ids)
						channels_new, _ := h.db.Groupchat.Query().All(context.Background())

						for _, channel_new := range channels_new {
							if channel_new.GroupName == request.ChannelName {
								channel_id = channel_new.ID

								_, errCreate := h.db.GroupMessage.Create().SetText(request.Text).SetTime(request.Time).SetReceivedID(channel_id).SetSentID(user.ID).Save(context.Background())
								if errCreate != nil {
									log.Fatal("Fail to add message to db: ", errCreate)
								}
							}

						}

						break
					}
					break

				}
				intTemp1 = intTemp1 + 1
			}
		
			fmt.Println("ủa alo: ", intTemp1)
			fmt.Println("ủa alo: ", len(users))
			if intTemp1 == len(users) {

				fmt.Println("ủa vô đây không ta")
				// insert user, insert channel, insert channel_message
				_, err_create_user := h.db.User.Create().SetUsername(request.Username).SetPassword("").Save(context.Background())

				if err_create_user != nil {
					log.Fatal("Fail to add user to db: ", err_create_user)
				}

				_, errCreateChannel := h.db.Groupchat.Create().SetGroupName(request.ChannelName).Save(context.Background())

				if errCreateChannel != nil {
					log.Fatal("Fail to add channel to db: ", errCreateChannel)
				}

				var channel_ids = ""
				channel_id, _ := strconv.Atoi(channel_ids)
				channels_new, _ := h.db.Groupchat.Query().All(context.Background())

				for _, channel_new := range channels_new {
					fmt.Println("channel_name:", channel_new.GroupName)
					fmt.Println("channel_name:", request.ChannelName)
					if channel_new.GroupName == request.ChannelName {
						channel_id = channel_new.ID
						break
					}
				}

				var user_ids = ""
				user_id, _ := strconv.Atoi(user_ids)
				users_new, _ := h.db.User.Query().All(context.Background())

				for _, user_new := range users_new {
					if user_new.Username == request.Username {
						user_id = user_new.ID
						break
					}
				}

				_, errCreate := h.db.GroupMessage.Create().SetText(request.Text).SetTime(request.Time).SetReceivedID(channel_id).SetSentID(user_id).Save(context.Background())
				if errCreate != nil {
					log.Fatal("Fail to add message to db: ", errCreate)
				}
			}
			
			var receive = pb.ChatReply{
				Username:    request.Username,
				ChannelName: request.ChannelName,
				Text:        request.Text,
				Time:        request.Time,
			}

			fmt.Printf("re %v", &receive)
			msg_receive, err2 := proto.Marshal(
				&pb.Messages{
					Payload: &pb.Messages_ChatReply{
						ChatReply: &receive,
					},
				})

			if err2 != nil {
				log.Fatal("Fail to encode message in run: ", err2)
			}
			var clientsChannel = h.channels[request.ChannelName] // find the list of clients
			var n = len(clientsChannel)
			fmt.Println("list client: %n", n)
			for i := 0; i < n; i++ {

				select {
				case clientsChannel[i].send <- msg_receive: //send message to the client
				default:
					close(clientsChannel[i].send)
					delete(h.clients, clientsChannel[i])
				}
			}
		}
	}

}
